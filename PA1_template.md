---
title: "Reproducible Research: Peer Assessment 1"
output: 
  html_document:
    keep_md: true
---


## Loading and preprocessing the data

```r
library(data.table)
library(lattice)
if (!file.exists("activity.csv")) unzip("activity.zip")  
data<-as.data.table(read.csv("activity.csv"))
data_no_na<-data[complete.cases(data),]
```

Data summary (without the incomplete cases):

```r
summary(data)
```

```
##      steps               date          interval   
##  Min.   :  0.0   2012-10-01:  288   Min.   :   0  
##  1st Qu.:  0.0   2012-10-02:  288   1st Qu.: 589  
##  Median :  0.0   2012-10-03:  288   Median :1178  
##  Mean   : 37.4   2012-10-04:  288   Mean   :1178  
##  3rd Qu.: 12.0   2012-10-05:  288   3rd Qu.:1766  
##  Max.   :806.0   2012-10-06:  288   Max.   :2355  
##  NA's   :2304    (Other)   :15840
```
## What is mean total number of steps taken per day?

Performed on the dataset with the missing values removed.

```r
per_day_no_na<-data_no_na[,lapply(.SD,sum),by=date]
hist(per_day_no_na$steps)
```

![plot of chunk unnamed-chunk-3](figure/unnamed-chunk-3.png) 

```r
mean(per_day_no_na$steps)
```

```
## [1] 10766
```

```r
median(per_day_no_na$steps)
```

```
## [1] 10765
```
## What is the average daily activity pattern?

```r
per_interval<-data_no_na[,lapply(.SD,mean),by=interval]
with(per_interval,plot(x=interval,y=steps,type="l"))
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-4.png) 

```r
max_steps<-per_interval$interval[which.max(per_interval$steps)]
print(max_steps)
```

```
## [1] 835
```

Maximum number of average daily steps take place on interval 835

## Imputing missing values

The number of cases with missing values is:

```r
sum(!complete.cases(data))
```

```
## [1] 2304
```

The missing values will be replaced by the average number of steps for that interval


```r
missing_intervals<-data[!complete.cases(data),interval]
missing_steps<-as.integer(round(per_interval$steps[match(missing_intervals,per_interval$interval)]))
data[!complete.cases(data),]$steps<-missing_steps
```

Now there are no cases with missing values:

```r
sum(!complete.cases(data))
```

```
## [1] 0
```

A new histogram of the total number of steps per day

```r
per_day<-data[,lapply(.SD,sum),by=date]
hist(per_day$steps)
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9.png) 

```r
mean(per_day$steps)
```

```
## [1] 10766
```

```r
median(per_day$steps)
```

```
## [1] 10762
```

The mean value is equal to the previously calculated value but the median is now lower.  
## Are there differences in activity patterns between weekdays and weekends?

```r
data$weekday<-factor(weekdays(as.Date(data$date)))
data$weekend<-factor(data$weekday%in%c("Saturday","Sunday"))
per_interval_weekend<-data[,lapply(.SD,mean),by=list(interval,weekend)]
levels(per_interval_weekend$weekend)<-c("Weekday","Weekend")
xyplot(steps ~ interval | weekend,data=per_interval_weekend,type="l",layout = c(2, 1))
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10.png) 
Fewer steps are taken in the weekend around 835.
